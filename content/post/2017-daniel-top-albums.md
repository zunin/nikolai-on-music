---
title: "2017 Daniel Top Albums"
date: 2019-02-03T21:28:35+01:00
draft: false
---
This list is my friend Daniel's picks for 2017 which he asked me to put out there.
Without further ado, here's the picks for best albums released in 2017 followed by honorable mentions.

# 5 Goldfish - Late Night People
{{< spotify/album 55b9QqaSziAJ53S63C3VYN >}}

# 4 David Maxim Micic - Who Bit the Moon
{{< spotify/album 4XlMNDp7GSFGF56dw5CJG5 >}}

# 3 Oh Wonder - Ultralife
{{< spotify/album 2P63KJNeC3DwaLpw8KHYqE >}}

# 2 Queens of the Stone Age - Villains
{{< spotify/album 6JdX9MGiEMypqYLMKyIE8a >}}


# 1 Seether - Poison the Parish
{{< spotify/album 2CbMgWfTfT4n80PjrDkbbt >}}


# Honorable mentions
## Foo Fighters - Concrete and Gold
{{< spotify/album 6KMkuqIwKkwUhUYRPL6dUc >}}

## Blackfield - Blackfield V
{{< spotify/album 0cjbx7Gz3KZaY3906LZTlP >}}


## Gorillaz - Humanz
{{< spotify/album 0NvirtaDCaZU5PAW1O5FDE >}}

## Amplifier - Trippin' With Dr. Faustus
{{< spotify/album 1fjtOXNqlSHFUsWCBsWjut >}}

## Dead Blonde Stars - Resolution (EP)
{{< bandcamp 3947326477 >}}

## Gone is Gone - Echolocation
{{< spotify/album 4Xnbjt8wACN0QyWqWp2mol >}}

## Steven Wilson - To The Bone
{{< spotify/album 1ahl6UmqCDuNy6rJVfRKeC >}}

## Wolf Alice - Visions of a Life
{{< spotify/album 4igFAe5sMPXBZRHj66tP8c >}}

## Nothing More - The Stories We Tell Ourselves
{{< spotify/album 4tEmy5QxiF1b65HxuGnkco >}}

## Comments

{{< comments >}}
