---
title: 2018 Top Albums
date: 2019-02-03T21:30:48.000+00:00

---
These are my picks for best 5 albums in 2018. We had some internal discussion in my friend group that not many good albums were released this year but thinking about it for a while, more and more good albums seemed to pop up. Here are my choices followed by honorable mentions.

# 5 Halestorm - Vicious

This album took me by storm and kind of after the fact of the release.
It's both seductive and aggressive and features amazing instrumentation
as well as vocals. Go give it a listen!

{{< spotify/album 6hIMdrqgLXY73T9411Y7Ux >}}

# 4 Ghost - Prequelle

This album actually has an interesting story in relation to the law
suit from the former band members of Ghost. Part of the suit is that
they provided lots of the identity of Ghost (to my understanding) and
this release is supposed to prove that they can indeed still make songs
without them. In my opinion they succeeded (yet since every album has
been different and this one is as well, how do you determine that?).
In either case it's a good album featuring a sax solo (bitchin'!) in
`Miasma`. I like how the album quotes itself musically though I can see
how that might seem "lazy" to some.

{{< spotify/album 1KMfjy6MmPorahRjxhTnxm >}}

# 3 A Perfect Circle - Eat the elephant

This album starts off strong and falls off by track six or seven.
But what a start. If it had kept up the pace there would have been
no competition for first place. I think the disgusting artwork is
amazing and disturbing and fits the general feel of the very ironically
toned `So Long, and Thanks For All The Fish` (obviously an homage to
hitchhiker's guide to the galaxy). I had the luck of being able
to catch them live this year and they performed incredibly well.

{{< spotify/album 3Jr1RhAyndBxtyi8rJs3Op >}}

# 2 The Pineapple Thief - Dissolution

I was excited to hear that this album was coming out but what excited me even
more was when I realized that Gavin Harrison was drumming on here. In particular
I think it's worth listening to `Try as I Might` and `Threatening War` as they
are some of the catchiest prog I have heard all of 2018. The entire album is
amazing though and I would recommend giving it a listen.

{{< spotify/album 5mKAoSFsmgLBjBZofrqkd3 >}}

# 1 Vulfpeck - Hill Climber

Over the years I've fallen deeper and deeper in love with Vulfpeck.
Yes, it's possible it's because I play bass and their bassist is
amazing but in reality the music is just very very good. This album
is no exception and `Lonely town` is such a compelling song.

{{< spotify/album 2cZ4rjMOn8zz1ToKSBQVWa >}}

# Honorable mentions

As is tradition, honorable mentions are albums I thought are worth
discussing and listening to but are presented in no particular order.

## First Aid Kid - Ruins

I only learned about this band this year. Absolutely stunning
album and while I dug deeper into live performanced I learned
that they perform lots of amazing covers live too. I think
they're a great band and worth a listen -- in particular I
am going to try to catch them live.

{{< spotify/album 5l2Ts5Hd4BN2O28rZksznR >}}

## Shinedown - ATTENTION ATTENTION

I have listened to so much Shinedown throughout the years and
I have some thoughts about this album in particular. Sorry for the
incoming rant.

Their 2015 album `Threat To Survival` only provided a few
hits to satisfy my need, it did not tick the boxes I had come
to expect from a Shinedown release. 2012's `Amaryllis` initially
felt like an amazing return to hard rock for Shinedown but over
time felt like an anti bully record for the children of the band
members, I did not mind too much as the melodies were on point and
did to some extent match the level of their magnus opus `The Sound Of Madness`
from 2008.

`ATTENTION ATTENTION` seems like them lyrically trying to distance
themselves from the `Amaryllis` "anti-bully" themes. The songs are
good but what is strange is the all capitalized song titles except of
course from `special` which is lower cased.

I would recommend listening to Shinedown and while I would not avoid
`ATTENTION ATTENTION` newcomers should seek `The Sound Of Madness` or
the live album `Somewhere In The Stratosphere`.

{{< spotify/album 0dtwIycvTaFNjo44QRwWz7 >}}

## Alice in Chains - Rainier Fog

Great album which I feel I haven't listened to enough
to give a chance. I feel like I breeze the few times
I've given the album a full listen.

{{< spotify/album 4AAPRl8BKlsIVC5aeedlBv >}}

## Clutch - Book of Bad Decisions

Clutch is band that a colleague of mine introduced me to
during 2017 and it suits my person very well. I look forward
to discovering their discography more in the future.

{{< spotify/album 4NhNjAi8FbwJQBsYf7BSfS >}}

## Greta Van Fleet - Anthem of the Peaceful Army

Greta Van Fleet has caught a lot of flack but I like them.
This current album is not as catchy as the last to me but
they are surely in my regular rotations of songs.

{{< spotify/album 7zeCZY6rQRufc8IHGKyXGX >}}

## Gorillaz - The Now Now

Had a few fun hits but not really for me, overall. Was
better than `Humanz` in my opinion.

{{< spotify/album 1amYhlukNF8WdaQC3gKkgL >}}

## Muse - Simulation Theory

This album is full of actual hits that I wouldn't necessarily
attribute to Muse but yet again it's unmistakably Muse. It is
very much in tune with their "tick-tock" cycle of experimental/synth
then more pure rock album cycle.

{{< spotify/album 5OZgDtx180ZZPMpm36J2zC >}}

## Slash - Living The Dream

I love both Myles Kennedy and Slash and I think that the
songwriting works well on this entire album. I can put it on
and rock out for the duration of the album without really
realizing what happened.

{{< spotify/album 1P3BCzFQl0Bx9fyjOFTFv9 >}}

## Comments

{{< comments >}}
