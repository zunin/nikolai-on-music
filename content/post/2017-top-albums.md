---
title: "2017 Top Albums"
date: 2019-02-03T16:28:43+01:00
draft: false
---
# Prelude
I'm going to start off by saying that this entry is made after the fact that the list was made. It was made originally as is tradition with two of my good music loving friends to get a conversation started and hopefully find some awesome music. I am going to cut right to the bone here and start off with the top 5 followed by a bunch of albums I thought was worth mentioning too.

# 5. Seether - Poison the Parish
So I've been a Seether fan for a while, but this album kind of surprised me when I heard the first single from it. It's definitely Seether and it's still great. I feel like it's a bit heavier than it's 2014 predecessor `Isolate and medicate` which I consider a masterpiece and which I believe is a better album. Not that I consider that a knock on `Poison the Parish` which still is a great album.

{{< spotify/album 2CbMgWfTfT4n80PjrDkbbt >}}

# 4. Mew - Visuals
This album has been one of the albums I have popped on while working and would cause me to have one of the beautiful melodies bouncing around my head for the rest of the day. For instance the song `85 videos` has a haunting chorus that usually fills that criteria.

{{< spotify/album 3oNtmmffPkYFDo6AhIrCil >}}

# 3. Vulfpeck - Mr Finish Line
This is the third iteration of the habit of releasing an album per year from American funk band Vulfpeck. And luckily for us the quality is not declining. This album is full of great hooks which I find myself humming, for example `Birds of a feather, we rock together` or `Mr. Finish Line`. A highlight is the surprisingly silly final song `Captain Hook` featuring "Baby Theo" which I assume is Theo Katzman with his voice pitch lifted that luckily never gets too annoying.

{{< spotify/album 631tfYWQsACU5Kmk8dpqli >}}

# 2. Queens of the Stone Age - Villains
Very good. I'm going to go ahead and say I don't think it was as good as the 2013 album `...Like Clockwork`, but it is very close. This album was a tight contender for best album as well! I was surprised to learn that Mark Ronson helped produce this album but in my head album it must have had affected songs like `The Way You Used to Do` which is insanely danceable, a trait not all Queens of the Stone Age songs share. I think the single low light is that I just can't seem to remember the song `Hideaway` if I left the album for a handful of weeks. It's not a bad song but it's not up to snuff in opinion. That doesn't ruin this amazing album though. 

{{< spotify/album 6JdX9MGiEMypqYLMKyIE8a >}}

# 1. Steven Wilson - To The Bone
I can't distinguish if this is either an amazing album or if I am too big of a fan boy, but this is the album I was both most excited for and one of the albums I heard the most this year. Thoroughly enjoyed it. I enjoyed the dark songs like `People Who Eat Darkness` as well as the Abba/Electric Light Orchestra hit pop feeling `Pariah`. I loved the continued collaboration with Ninet Tayeb whose vocals are amazing yet again.

{{< spotify/album 1ahl6UmqCDuNy6rJVfRKeC >}}

# Honorable mentions
As this was a good year for music I find it worth mentioning quite a few albums. Here they are in no particular order with a few sentences attached. There's a lot -- I'm sorry!

## Kellermensch - Goliath
I was so surprised by this album as I wasn't really aware of (thanks Daniel!). The songs hooked me instantly and were well produced.

{{< spotify/album 1dSqSHeaDTIQnKw7EKebA9 >}}

## Lorde - Melodrama
I discovered this album mostly due to the podcast songexploder in which [episode 118 featured "Sober"](http://songexploder.net/lorde). The song grew on me and I started listening to the rest of the album which is great.

{{< spotify/album 4oCGmYsAQOWt2ACWTpNUU6 >}}

## Sia - Everyday is Christmas
I like Sia. I like Christmas music too, so this seems dead simple. I was surprised to have new christmas-y music that isn't just a rehash of old songs.

{{< spotify/album 0DB01cPUt66gHPLL1JcdQq >}}

## Miley Cyrus - Younger now
It's a style of Miley Cyrus that suits me much better than the few albums before it, namely a more rock-ish and country-ish sound. I think it's pretty solid with decent highlights like `Younger now`, `Malibu` and the duet with Dolly Parton `Rainbowland` .

{{< spotify/album 5xG9gJcs9ut3qDWezHUlsX >}}

## Blackfield - Blackfield V
So I'm a sucker for Steven Wilson, sue me. This album is very solid to back me up though. It's more ambient and pop friendly than most of Steven Wilson though.

{{< spotify/album 0cjbx7Gz3KZaY3906LZTlP >}}

## Amplifier - Trippin' with Dr Faustus
The only reason this album is not on the top 5 is that I can't seem to remember the names of the songs enough so I don't feel like it impacted me. I've put it on so many times and just blown through it wondering where my time went.

{{< spotify/album 1fjtOXNqlSHFUsWCBsWjut >}}

## Foo Fighters - Concrete and Gold
A lot of memorable moments on this album. It's pretty good and feels a tad experimental for a Foo Fighters album which I like. But there's also a few duds in there. I remember `Run` being released as a single with a music video and it was kind of weird. The song is probably the best on the album though.

{{< spotify/album 6KMkuqIwKkwUhUYRPL6dUc >}}

## Nine inch Nails - Add Violence
Technically an LP? Whatever, the single off it `Less Than` is amazing and the closing track `The Background world` is a great song which features a catchy hook that in the back half of the song distorts for minutes continuously. I usually don't feel like being too pretentious about these things but I had been working with sound and distortion a lot in school so forgive me. The experience of listening through than and then realizing when the music turned into noise was a very interesting experience to me. For subsequent listens I skip the end. 

{{< spotify/album 28DJ00Yr5oOhH0uOUgTQwc >}}

## The Darkness - Pinewood Smile
Solid album, frankly. Still not taking themselves too seriously but I feel like it was a step backwards compared to `Last of our kind` from 2015.


{{< spotify/album 4wWnahYVzAtXl7JFELH3ng >}}

## Steel Panther - Lower the bar
Pretty good album with bitchin' licks in every song. If you never heard about Steel Panther consider this a NSFW warning. It might be the best album since `Feel the steel` from 2009, but I do feel like Steel Panther are best consumed in a "best of" kind of format.

{{< spotify/album 4PLUZDe6PFhR5JURlaaWRN >}}

## Neil Cicierega - Mouth Moods
Oh boy, I was laughing and jamming my ass off for this one. One of the best mashup albums I have ever heard. The third song `AC/VC` is an absolute classic and is a must listen in my book.

{{< soundcloud 304209955 >}}

## Comments

{{< comments >}}

